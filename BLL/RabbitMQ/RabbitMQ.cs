﻿using RabbitMQ.Client;
using System.Diagnostics;

namespace BLL.RabbitMQ
{

    public interface IRabbitMqService
    {
        void SendFinishMessage(string message);
        void SendCommandMessage(string message, int idCommand);
    }

    public class RabbitMqService : IRabbitMqService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public RabbitMqService(IConnection connection, IModel channel)
        {
            Debug.WriteLine("RabbitMqService CONSTRUCTEUR");

            Debug.WriteLine("connection : " + connection);
            Debug.WriteLine("channel : " + channel);
            Debug.WriteLine("queue: \"mailing\", durable: true, exclusive: false, autoDelete: false, arguments: null");

            _connection = connection;
            _channel = channel;

            _channel.QueueDeclare(queue: "mailing", durable: true, exclusive: false, autoDelete: false, arguments: null);
        }

        public void SendFinishMessage(string _email)
        {
            var messageObject = new
            {
                command = "sendDeliveryFinishedEmail",
                myObject = new
                {
                    email = _email
                }
            };

            string lastmessage = Newtonsoft.Json.JsonConvert.SerializeObject(messageObject);
            var body = System.Text.Encoding.UTF8.GetBytes(lastmessage);

            try
            {
                _channel.BasicPublish(exchange: "", routingKey: "mailing", basicProperties: null, body: body);
                Console.WriteLine("Message envoyé ?");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.StackTrace}");
                throw;
            }
            Console.WriteLine("Message envoyé : {0}", _email);
        }

        public void SendCommandMessage(string _email, int idCommand)
        {
            var messageObject = new
            {
                command = "sendCommandEmail",
                myObject = new
                {
                    email = _email,
                    IdCommand = idCommand
                }
            };

            string lastmessage = Newtonsoft.Json.JsonConvert.SerializeObject(messageObject);
            var body = System.Text.Encoding.UTF8.GetBytes(lastmessage);

            try
            {
                _channel.BasicPublish(exchange: "", routingKey: "mailing", basicProperties: null, body: body);
                Console.WriteLine("Message envoyé ?");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.StackTrace}");
                throw;
            }
            Console.WriteLine("Message envoyé : {0}", _email);
        }
    }

}
