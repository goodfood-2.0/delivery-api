﻿using BLL.Interfaces;
using DAL.Entity;
using DAL.Interfaces;

namespace BLL.Services
{
    public class StatusesDeliveriesService : IStatusesDeliveriesService
    {
        public IStatusesDeliveriesOperation _statusesDeliveriesOperation;

        public StatusesDeliveriesService(IStatusesDeliveriesOperation statusesDeliveriesOperation)
        {
            _statusesDeliveriesOperation = statusesDeliveriesOperation;
        }

        public async Task<IEnumerable<StatusesDelivery>> GetStatusesDeliveriesAsync()
        {
            return await _statusesDeliveriesOperation.GetStatusesDeliveriesAsync();
        }

        public async Task<StatusesDelivery> GetStatusesDeliveryByIdAsync(int id)
        {
            return await _statusesDeliveriesOperation.GetStatusesDeliveryByIdAsync(id);
        }

        public async Task CreateStatusesDeliveryAsync(StatusesDelivery statusesDeliveries)
        {
            await _statusesDeliveriesOperation.CreateStatusesDeliveryAsync(statusesDeliveries);
        }

        public async Task DeleteStatusesDeliveryAsync(int id)
        {
            await _statusesDeliveriesOperation.DeleteStatusesDeliveryAsync(id);
        }

        public async Task<StatusesDelivery> GetStatusesDeliveryByDesciptionAsync(string desc)
        {
            return await _statusesDeliveriesOperation.GetStatusesDeliveryByDesciptionAsync(desc);
        }
    }
}
