﻿using Azure.Storage.Blobs;
using BLL.Interfaces;
using BLL.RabbitMQ;
using DAL.Entity;
using DAL.Interfaces;
using Microsoft.AspNetCore.Http;

namespace BLL.Services
{
    public class DeliveriesService : IDeliveriesService
    {
        public IDeliveriesOperation _deliveriesOperation;
        public IStatusesDeliveriesOperation _statusesDeliveriesOperation;
        public IRabbitMqService _rabbitMqService;

        private readonly BlobServiceClient _blobServiceClient;
        private const string containerName = "images";

        public DeliveriesService(IDeliveriesOperation deliveriesOperation, IStatusesDeliveriesOperation statusesDeliveriesOperation, IRabbitMqService rabbitMqService, BlobServiceClient blobServiceClient)
        {
            _deliveriesOperation = deliveriesOperation;
            _statusesDeliveriesOperation = statusesDeliveriesOperation;
            _rabbitMqService = rabbitMqService;

            _blobServiceClient = blobServiceClient;
        }

        public async Task<IEnumerable<Deliveries>> GetDeliveriesAsync()
        {
            return await _deliveriesOperation.GetDeliveriesAsync();
        }

        public async Task<Deliveries> GetDeliveryByIdAsync(int id)
        {
            return await _deliveriesOperation.GetDeliveryByIdAsync(id);
        }

        public async Task<IEnumerable<Deliveries>> GetDeliveriesFromRestaurantId(int Id, string status)
        {
            return await _deliveriesOperation.GetDeliveriesFromRestaurantId(Id, status);
        }

        public async Task<List<Deliveries>> GetDeliveriesByOrderIdAsync(int[] ids)
        {
            return await _deliveriesOperation.GetDeliveriesByOrderIdsAsync(ids);
        }

        public async Task<List<Deliveries>> GetDeliveriesByEmailAsync(string email)
        {
            return await _deliveriesOperation.GetDeliveriesByEmailAsync(email);
        }

        public async Task CreateDeliveryAsync(Deliveries deliveries)
        {
            StatusesDelivery tempStatus = await _statusesDeliveriesOperation.GetStatusesDeliveryByDesciptionAsync("En cours");
            deliveries.StatusId = tempStatus.Id;
            deliveries.Status = tempStatus;

            await _deliveriesOperation.CreateDeliveryAsync(deliveries);
        }

        public async Task UpdateDeliveryAsync(int id, Deliveries deliveries)
        {
            await _deliveriesOperation.UpdateDeliveryAsync(id, deliveries);
        }

        public async Task DeleteDeliveryAsync(int id)
        {
            await _deliveriesOperation.DeleteDeliveryAsync(id);
        }

        public async Task ChangeStatusAsync(int id, string statusesDeliveries, IFormFile? file)
        {
            Deliveries tempDeliveries = await GetDeliveryByIdAsync(id);
            StatusesDelivery tempStatus;
            try
            {
                tempStatus = await _statusesDeliveriesOperation.GetStatusesDeliveryByDesciptionAsync(statusesDeliveries);
            }
            catch (Exception ex)
            {
                throw new Exception("Status not found \n" + ex.Message);
            }


            if (statusesDeliveries != tempDeliveries.Status!.MyStatus && statusesDeliveries == "Livré" && file != null)
            {
                var client = _blobServiceClient.GetBlobContainerClient(containerName);
                await client.CreateIfNotExistsAsync();

                var blobClient = client.GetBlobClient(file.FileName);

                using var stream = new MemoryStream();
                await file.CopyToAsync(stream);
                stream.Position = 0;

                await blobClient.UploadAsync(stream, true);
                string uri = blobClient.Uri != null ? blobClient.Uri.ToString() : "Error";

                tempDeliveries.PicturePath = uri;
            }

            if (statusesDeliveries == "Validée")
            {
                _rabbitMqService.SendCommandMessage(tempDeliveries.Email!, tempDeliveries.Id);
            }

            if (statusesDeliveries == "Livré")
            {
                _rabbitMqService.SendFinishMessage(tempDeliveries.Email!);
            }

            tempDeliveries.StatusId = tempStatus.Id;
            await UpdateDeliveryAsync(tempDeliveries.Id, tempDeliveries);
        }


    }
}
