﻿using DAL.Entity;
using Microsoft.AspNetCore.Http;

namespace BLL.Interfaces
{
    public interface IDeliveriesService
    {
        Task<IEnumerable<Deliveries>> GetDeliveriesAsync();
        Task<Deliveries> GetDeliveryByIdAsync(int id);
        Task CreateDeliveryAsync(Deliveries deliveries);
        Task UpdateDeliveryAsync(int id, Deliveries deliveries);
        Task DeleteDeliveryAsync(int id);
        Task ChangeStatusAsync(int id, string deliveries, IFormFile? file = null);
        Task<IEnumerable<Deliveries>> GetDeliveriesFromRestaurantId(int Id, string status);
        Task<List<Deliveries>> GetDeliveriesByOrderIdAsync(int[] ids);
        Task<List<Deliveries>> GetDeliveriesByEmailAsync(string email);
    }
}
