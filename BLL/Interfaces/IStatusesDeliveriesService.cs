﻿using DAL.Entity;

namespace BLL.Interfaces
{
    public interface IStatusesDeliveriesService
    {
        Task<IEnumerable<StatusesDelivery>> GetStatusesDeliveriesAsync();
        Task<StatusesDelivery> GetStatusesDeliveryByIdAsync(int id);
        Task CreateStatusesDeliveryAsync(StatusesDelivery deliveries);
        Task DeleteStatusesDeliveryAsync(int id);
        Task<StatusesDelivery> GetStatusesDeliveryByDesciptionAsync(string desc);
    }
}
