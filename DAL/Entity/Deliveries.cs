﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entity
{
    public class Deliveries
    {
        [Key]
        public int Id { get; set; }
        public int StatusId { get; set; }
        public StatusesDelivery? Status { get; set; }
        public int? Id_Restaurant { get; set; }
        public int? Id_Order { get; set; }
        public string? Email { get; set; }
        public string? PicturePath { get; set; }
    }
}
