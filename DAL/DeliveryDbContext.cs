﻿using DAL.Entity;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class DeliveryDbContext : DbContext
    {

        public DbSet<Deliveries> Deliveries { get; set; }
        public DbSet<StatusesDelivery> StatusesDelivery { get; set; }

        public DeliveryDbContext(DbContextOptions<DeliveryDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Deliveries>().HasKey(x => x.Id);

            modelBuilder.Entity<Deliveries>()
            .HasOne(d => d.Status)
            .WithMany()
            .HasForeignKey(d => d.StatusId);


            modelBuilder.Entity<StatusesDelivery>().HasKey(x => x.Id);
        }
    }
}
