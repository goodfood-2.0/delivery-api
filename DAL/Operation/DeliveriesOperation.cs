﻿using DAL.Entity;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Operation
{
    public class DeliveriesOperation : IDeliveriesOperation
    {
        private readonly DeliveryDbContext _context;

        public DeliveriesOperation(DeliveryDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Deliveries>> GetDeliveriesAsync()
        {
            return await _context.Deliveries.Include(x => x.Status).ToListAsync();
        }

        public async Task<Deliveries> GetDeliveryByIdAsync(int id)
        {
            return await _context.Deliveries.Include(x => x.Status).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<Deliveries>> GetDeliveriesByOrderIdsAsync(int[] ids)
        {
            return await _context.Deliveries.Include(x => x.Status).Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<List<Deliveries>> GetDeliveriesByEmailAsync(string email)
        {
            return await _context.Deliveries.Include(x => x.Status).Where(x => x.Email == email).ToListAsync();
        }

        public async Task<IEnumerable<Deliveries>> GetDeliveriesFromRestaurantId(int Id, string status)
        {
            return await _context.Deliveries.Include(x => x.Status)
                .Where(x => x.Id_Restaurant == Id)
                .Where(x => status != null ? x.Status.MyStatus == status : true)
                .ToListAsync();
        }
        public async Task CreateDeliveryAsync(Deliveries deliveries)
        {
            _context.Deliveries.Add(deliveries);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateDeliveryAsync(int id, Deliveries deliveries)
        {
            var existingDelivery = await _context.Deliveries.FindAsync(id);

            if (existingDelivery != null)
            {
                existingDelivery.StatusId = deliveries.StatusId;
                existingDelivery.Id_Order = deliveries.Id_Order;
                existingDelivery.PicturePath = deliveries.PicturePath;

                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteDeliveryAsync(int id)
        {
            var existingDelivery = await _context.Deliveries.FindAsync(id);

            if (existingDelivery != null)
            {
                _context.Deliveries.Remove(existingDelivery);
                await _context.SaveChangesAsync();
            }
        }
    }
}

