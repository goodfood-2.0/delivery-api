﻿using DAL.Entity;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Operation
{
    public class StatusesDeliveriesOperation : IStatusesDeliveriesOperation
    {
        private readonly DeliveryDbContext _context;

        public StatusesDeliveriesOperation(DeliveryDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<StatusesDelivery>> GetStatusesDeliveriesAsync()
        {
            return await _context.StatusesDelivery.ToListAsync();
        }

        public async Task<StatusesDelivery> GetStatusesDeliveryByIdAsync(int id)
        {
            return await _context.StatusesDelivery.FindAsync(id);
        }

        public async Task<StatusesDelivery> GetStatusesDeliveryByStatus(string status)
        {
            return await _context.StatusesDelivery.Where(x => x.MyStatus == status).FirstOrDefaultAsync();
        }

        public async Task CreateStatusesDeliveryAsync(StatusesDelivery deliveries)
        {
            _context.StatusesDelivery.Add(deliveries);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteStatusesDeliveryAsync(int id)
        {
            var existingDelivery = await _context.StatusesDelivery.FindAsync(id);

            if (existingDelivery != null)
            {
                _context.StatusesDelivery.Remove(existingDelivery);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<StatusesDelivery> GetStatusesDeliveryByDesciptionAsync(string desc)
        {
            return await _context.StatusesDelivery.Where(x => x.MyStatus == desc).FirstOrDefaultAsync();
        }
    }

}

