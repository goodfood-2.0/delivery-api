﻿using DAL.Entity;

namespace DAL.SeedData
{
    public static class SeedData
    {
        public static void Initialize(DeliveryDbContext context)
        {
            if (!context.StatusesDelivery.Any())
            {
                context.StatusesDelivery.AddRange(
                    new StatusesDelivery { MyStatus = "Créée" },
                    new StatusesDelivery { MyStatus = "En cours" },
                    new StatusesDelivery { MyStatus = "Validée" },
                    new StatusesDelivery { MyStatus = "Livré" }
                );

                context.SaveChanges();
            }
        }
    }
}
