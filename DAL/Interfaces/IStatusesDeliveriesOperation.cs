﻿using DAL.Entity;

namespace DAL.Interfaces
{
    public interface IStatusesDeliveriesOperation
    {
        public Task<IEnumerable<StatusesDelivery>> GetStatusesDeliveriesAsync();
        public Task<StatusesDelivery> GetStatusesDeliveryByIdAsync(int id);
        public Task CreateStatusesDeliveryAsync(StatusesDelivery deliveries);
        public Task DeleteStatusesDeliveryAsync(int id);
        public Task<StatusesDelivery> GetStatusesDeliveryByDesciptionAsync(string desc);
    }

}
