﻿using DAL.Entity;

namespace DAL.Interfaces
{
    public interface IDeliveriesOperation
    {
        public Task<IEnumerable<Deliveries>> GetDeliveriesAsync();
        public Task<Deliveries> GetDeliveryByIdAsync(int id);
        public Task CreateDeliveryAsync(Deliveries deliveries);
        public Task UpdateDeliveryAsync(int id, Deliveries deliveries);
        public Task DeleteDeliveryAsync(int id);
        Task<IEnumerable<Deliveries>> GetDeliveriesFromRestaurantId(int Id, string status);
        Task<List<Deliveries>> GetDeliveriesByOrderIdsAsync(int[] ids);
        Task<List<Deliveries>> GetDeliveriesByEmailAsync(string email);
    }

}
