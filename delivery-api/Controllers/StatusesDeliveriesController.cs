﻿using BLL.Interfaces;
using DAL.Entity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivery_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StatusesDeliveriesController : ControllerBase
    {
        private readonly IStatusesDeliveriesService _statusesDeliveryService;

        public StatusesDeliveriesController(IStatusesDeliveriesService statusesDeliveryService)
        {
            _statusesDeliveryService = statusesDeliveryService;
        }

        // GET: api/Deliveries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatusesDelivery>>> GetStatusesDeliveries()
        {
            var deliveries = await _statusesDeliveryService.GetStatusesDeliveriesAsync();
            return Ok(deliveries);
        }

        // GET: api/Deliveries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StatusesDelivery>> GetStatusesDeliveries(int id)
        {
            var deliveries = await _statusesDeliveryService.GetStatusesDeliveryByIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/Deliveries/5
        [HttpGet("Status/{status}")]
        public async Task<ActionResult<StatusesDelivery>> GetStatusesDeliveries(string status)
        {
            var deliveries = await _statusesDeliveryService.GetStatusesDeliveryByDesciptionAsync(status);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // POST: api/Deliveries
        [HttpPost]
        public async Task<ActionResult<StatusesDelivery>> PostStatusesDeliveries(StatusesDelivery statusesDelivery)
        {
            await _statusesDeliveryService.CreateStatusesDeliveryAsync(statusesDelivery);

            return CreatedAtAction("GetStatusesDeliveries", new { id = statusesDelivery.Id }, statusesDelivery);
        }

        // DELETE: api/Deliveries/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDeliveries(int id)
        {
            var deliveries = await _statusesDeliveryService.GetStatusesDeliveryByIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            await _statusesDeliveryService.DeleteStatusesDeliveryAsync(id);

            return NoContent();
        }
    }
}
