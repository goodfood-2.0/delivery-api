﻿using BLL.Interfaces;
using DAL.Entity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivery_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DeliveriesController : ControllerBase
    {
        private readonly IDeliveriesService _deliveryService;

        public DeliveriesController(IDeliveriesService deliveryService)
        {
            _deliveryService = deliveryService;
        }

        // GET: api/Deliveries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Deliveries>>> GetDeliveries()
        {
            var deliveries = await _deliveryService.GetDeliveriesAsync();
            return Ok(deliveries);
        }

        // GET: api/Deliveries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Deliveries>> GetDeliveries(int id)
        {

            var deliveries = await _deliveryService.GetDeliveryByIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/Deliveries/5,6
        [HttpGet("byOrderId/{ids}")]
        public async Task<ActionResult<List<Deliveries>>> GetDeliveriesByOrderId(string ids)
        {
            var idArray = ids.Split(',').Select(int.Parse).ToArray();

            var deliveries = await _deliveryService.GetDeliveriesByOrderIdAsync(idArray);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/Deliveries/patate.po@blabla.com
        [HttpGet("byEmail/{email}")]
        public async Task<ActionResult<List<Deliveries>>> GetDeliveriesByEmail(string email)
        {

            var deliveries = await _deliveryService.GetDeliveriesByEmailAsync(email);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/Deliveries/5
        [HttpGet("Restaurant/{Id}")]
        public async Task<ActionResult<IEnumerable<Deliveries>>> GetDeliveriesFromRestaurantId(int Id, string? status = null)
        {

            var deliveries = await _deliveryService.GetDeliveriesFromRestaurantId(Id, status!);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // PUT: api/Deliveries/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDeliveries(int id, Deliveries deliveries)
        {
            if (id != deliveries.Id)
            {
                return BadRequest();
            }

            await _deliveryService.UpdateDeliveryAsync(id, deliveries);

            return NoContent();
        }

        // PUT: api/Deliveries/5
        [HttpPut("ChangeStatus")]
        public async Task<IActionResult> ChangeStatus(int id, string deliveriesStatus, IFormFile? file = null)
        {
            await _deliveryService.ChangeStatusAsync(id, deliveriesStatus, file);

            return NoContent();
        }


        // POST: api/Deliveries
        [HttpPost]
        public async Task<ActionResult<Deliveries>> PostDeliveries(Deliveries deliveries)
        {
            await _deliveryService.CreateDeliveryAsync(deliveries);

            return CreatedAtAction("GetDeliveries", new { id = deliveries.Id }, deliveries);
        }

        // DELETE: api/Deliveries/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDeliveries(int id)
        {
            var deliveries = await _deliveryService.GetDeliveryByIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            await _deliveryService.DeleteDeliveryAsync(id);

            return NoContent();
        }
    }
}
