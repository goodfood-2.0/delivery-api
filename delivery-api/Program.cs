using Azure.Storage.Blobs;
using BLL.Interfaces;
using BLL.RabbitMQ;
using BLL.Services;
using DAL;
using DAL.Interfaces;
using DAL.Operation;
using DAL.SeedData;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using RabbitMQ.Client;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace delivery_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();

            // Add services to the container.
            builder.Services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                               .AllowAnyMethod()
                               .AllowAnyHeader();
                    });
            });

            string? connectionStringPostgres = "";
            string? connectionStringAzure = "";
            string RABBITMQ_HOSTNAME = "";
            int RABBITMQ_PORT = 5672;

            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
            {
                string? POSTGRES_USERNAME = Environment.GetEnvironmentVariable("POSTGRES_USERNAME");
                string? POSTGRES_PASSWORD = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
                string? POSTGRES_HOST = Environment.GetEnvironmentVariable("POSTGRES_HOST");
                string? POSTGRES_PORT = Environment.GetEnvironmentVariable("POSTGRES_PORT");
                string? POSTGRES_DATABASE = Environment.GetEnvironmentVariable("POSTGRES_DATABASE");

                connectionStringPostgres = $"Host={POSTGRES_HOST};Port={POSTGRES_PORT};Database={POSTGRES_DATABASE};Username={POSTGRES_USERNAME};Password={POSTGRES_PASSWORD}";

                RABBITMQ_HOSTNAME = Environment.GetEnvironmentVariable("RABBITMQ_HOSTNAME");
                RABBITMQ_PORT = int.Parse(Environment.GetEnvironmentVariable("RABBITMQ_PORT"));


                string? AZURE_DEFAULTENDPOINTSPROTOCOL = Environment.GetEnvironmentVariable("AZURE_DEFAULTENDPOINTSPROTOCOL") != null ? Environment.GetEnvironmentVariable("AZURE_DEFAULTENDPOINTSPROTOCOL") : "https";
                string? AZURE_ACCOUNTNAME = Environment.GetEnvironmentVariable("AZURE_ACCOUNTNAME");
                string? AZURE_ACCOUNTKEY = Environment.GetEnvironmentVariable("AZURE_ACCOUNTKEY");
                string? AZURE_ENDPOINTSUFFIX = Environment.GetEnvironmentVariable("AZURE_ENDPOINTSUFFIX") != null ? Environment.GetEnvironmentVariable("AZURE_ENDPOINTSUFFIX") : "core.windows.net";

                connectionStringAzure = $"DefaultEndpointsProtocol={AZURE_DEFAULTENDPOINTSPROTOCOL};AccountName={AZURE_ACCOUNTNAME};AccountKey={AZURE_ACCOUNTKEY};EndpointSuffix={AZURE_ENDPOINTSUFFIX}";
            }
            else
            {
                var configuration = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json")
                  .Build();

                connectionStringPostgres = configuration.GetConnectionString("DBDefaultConnection");
                Console.WriteLine(connectionStringPostgres);
                var rabbitMqSettings = configuration.GetSection("RabbitMqSettings");
                RABBITMQ_HOSTNAME = rabbitMqSettings["HostName"];
                RABBITMQ_PORT = int.Parse(rabbitMqSettings["Port"]);

                connectionStringAzure = configuration.GetConnectionString("AzureBlobStorage");
            }

            builder.Services.AddSingleton<IRabbitMqService>(sp =>
            {
                var factory = new ConnectionFactory() { HostName = RABBITMQ_HOSTNAME, Port = RABBITMQ_PORT };
                var connection = factory.CreateConnection();
                var channel = connection.CreateModel();

                return new RabbitMqService(connection, channel);
            });

            builder.Services.AddSingleton(x => new BlobServiceClient(connectionStringAzure));

            builder.Services.AddDbContext<DeliveryDbContext>(options => options.UseNpgsql(connectionStringPostgres));


            using (var scope = builder.Services.BuildServiceProvider().CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DeliveryDbContext>();
                context.Database.Migrate();

                SeedData.Initialize(context);
            }


            builder.Services.AddScoped<IStatusesDeliveriesService, StatusesDeliveriesService>();
            builder.Services.AddScoped<IDeliveriesService, DeliveriesService>();

            builder.Services.AddScoped<IStatusesDeliveriesOperation, StatusesDeliveriesOperation>();
            builder.Services.AddScoped<IDeliveriesOperation, DeliveriesOperation>();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Delivery", Version = "v1" });

                if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
                {
                    c.DocumentFilter<SwaggerDocumentFilter>();
                }
            });

            var app = builder.Build();

            app.UseCors("AllowAll");


            // Configure the HTTP request pipeline.
            if (Environment.GetEnvironmentVariable("ENV") != "PROD")
            {

                app.UseSwagger();
                app.UseSwaggerUI();
                Console.WriteLine("Environnement : " + Environment.GetEnvironmentVariable("ENV"));
                Console.WriteLine("Postgres string : " + connectionStringPostgres);
                Console.WriteLine("RabbitMQ name : " + RABBITMQ_HOSTNAME + "port :" + RABBITMQ_PORT);
                Console.WriteLine("Azure string : " + connectionStringAzure);
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }

    public class SwaggerDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            // Pr�fixe � ajouter aux chemins des appels HTTP
            string pathPrefix = "/delivery";

            var modifiedPaths = new Dictionary<string, OpenApiPathItem>();

            foreach (var pathItem in swaggerDoc.Paths)
            {
                var newPathItem = new OpenApiPathItem();

                foreach (var operation in pathItem.Value.Operations)
                {
                    newPathItem.AddOperation(operation.Key, operation.Value);
                }

                modifiedPaths[pathPrefix + pathItem.Key] = newPathItem;
            }

            // Ajouter les chemins modifi�s au document Swagger
            foreach (var modifiedPath in modifiedPaths)
            {
                swaggerDoc.Paths[modifiedPath.Key] = modifiedPath.Value;
            }

            // Supprimer les anciens chemins
            foreach (var pathItemKey in modifiedPaths.Keys)
            {
                swaggerDoc.Paths.Remove(pathItemKey.Substring(pathPrefix.Length));
            }
        }
    }
}